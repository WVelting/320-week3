using System;
using System.Net.Sockets;
using System.Text;
using TMPro;
using UnityEngine;

public class SocketToServer : MonoBehaviour
{

    public string Host = "127.0.0.1";
    public ushort Port = 8080; //0-65,535
    public TextMeshProUGUI ChatDisplay;
    public TMP_InputField InputDisplay;

    private TcpClient socket = new TcpClient();



    // Start is called before the first frame update
    void Start()
    {
        InputDisplay.interactable = false;
        InputDisplay.onEndEdit.AddListener(OnInputFieldEdit);
        ConnectToServer();

    }

    private async void ConnectToServer()
    {
        try
        {
            await socket.ConnectAsync(Host, Port);
            AddMessageToChatDisplay("<color=#005500>Successfully connected to Server</color>");
            InputDisplay.interactable = true;

        }
        catch (Exception e)
        {
            AddMessageToChatDisplay("<color=#550000>Error: " + e.Message +"</color>");
            return;

        }

        while (true)
        {
            byte [] data = new byte[socket.Available];
            await socket.GetStream().ReadAsync(data, 0, data.Length);
            if (data.Length > 0)
            {
                AddMessageToChatDisplay(Encoding.ASCII.GetString(data));
            }
        }

    }
    public void AddMessageToChatDisplay(string txt)
    {
        ChatDisplay.text += txt + "\n";

    }

    public void OnInputFieldEdit(string s)
    {
        SendMessageToServer(s);
        InputDisplay.text = "";
        InputDisplay.Select();
        InputDisplay.ActivateInputField();

    }

    public void SendMessageToServer(string message)
    {
        byte [] data = Encoding.ASCII.GetBytes(message);
        socket.GetStream().Write(data, 0, data.Length);
    }

}