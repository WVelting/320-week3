const net = require('net');

//represents our protocol
//Packets from server to Client
const Packet = {
    charEndOfPacket: '\n',
    charDelimeter: '\t',
    buildchat: function(usernameFrom, message){
        return this.buildFromParts(['CHAT',usernameFrom,message]);

    },
    buildAnnouncement: function(message){
        return this.buildFromParts(['ANNC', message]);

    },
    buildNameOkay: function(){
        return this.buildFromParts(['NOKY']);

    },
    buildNameBad: function(error){
        return this.buildFromParts(['NBAD', error]);

    },
    buildDM: function(sender, message){
        return this.buildFromParts(['DMSG',sender,message]);

    },
    buildList: function(arrayOfClients){
        const arrayOfUsernames = [];

        arrayOfClients.forEach( c => {
            if(c.username) arrayOfUsernames.push(c.username);
            else arrayOfUsernames.push(c.socket.localAddress);
        });

        arrayOfUsernames.unshift('LIST');
        return this.buildFromParts(arrayOfUsernames);

    },
    buildFromParts: function(arr){
        return arr.join(this.charDelimeter) + this.charEndOfPacket;
    }
}
class Client{

    constructor(socket, server)
    {
        this.buffer = '';
        this.username = '';
        this.socket = socket;
        this.server = server;
        this.socket.on('error', e => this.onError(e));
        this.socket.on('close', (b) => this.onDisconnect(b));
        this.socket.on('data', (d) => this.onData(d));

    }

    onError(err)
    {
        console.log('ERROR with ' + this.socket.localAddress + ' : ' + err);
    }

    onDisconnect(hadError)
    {
        this.server.onClientDisconnect(this);

    }

    onData(data)
    {
        this.buffer += data;

        //split our buffer apart into array of "packets"
        const packets = this.buffer.split('\n');

        this.buffer = packets.pop();

        console.log(packets.length + ' new packets received from ' + this.socket.localAddress);

        //handle all COMPLETE packets
        packets.forEach( p => this.handlePacket(p));

    }

    handlePacket(packet){

        //split our packet into parts
        const parts = packet.split('\t');

        //packets from client to server
        switch(parts[0]){
            case 'CHAT':
                server.broadcast(Packet.buildchat(this.username, parts[1]));
                break;
            case 'DMSG':
                //TODO: make it work
                break;
            case 'NAME':
                const newName = parts[1];

                //TODO: accept or reject new name
                this.username = newName;
                this.sendPacket(Packet.buildNameOkay());

                //TODO: send LIST packet to all users
                break;
            case 'LIST':

            this.sendPacket(Packet.buildList(this.server.clients));
                break;
        }
    }

    //packets from server to client
    sendPacket(packet){
        this.socket.write(packet);
    }

}

class Server{
    
    constructor()
    {
        this.port = 8080
        this.clients = [];

        this.socket = net.createServer({}, c => this.onClientConnect(c));
        this.socket.on("error", e => this.onError(e));
        this.socket.listen({port: this.port}, () => this.onStartListen());

    }

    onError(errMsg)
    {
        console.log("ERROR: " + errMsg);
    }

    onClientConnect(socketToClient)
    {
        console.log('A new client connect from ' + socketToClient.localAddress);
        const client = new Client(socketToClient, this);

        this.clients.push(client);

    }

    onClientDisconnect(client)
    {
        this.clients.splice(this.clients.indexOf(client), 1);

    }

    onStartListen()
    {
        console.log('The server is now listening on port ' +this.port);

    }
    broadcast(packet){
        this.clients.forEach(c => {
            c.sendPacket(packet)
        });
    }
}

const server = new Server();